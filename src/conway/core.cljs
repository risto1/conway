(ns conway.core
  (:require
   [clojure.set :refer [union difference]]
   ["file-saver" :as file-saver]))

(def live-cells (atom #{}))
(def context (atom nil))
(def buttons (atom {}))
(def simulation (atom nil))
(def simulation-speed 500) ;milliseconds

;Any live cell with fewer than two live neighbours dies, as if by underpopulation.
;Any live cell with two or three live neighbours lives on to the next generation.
;Any live cell with more than three live neighbours dies, as if by overpopulation.
;Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

(defn live-cell? [live-cells coordinates]
  (live-cells coordinates))

(defn delta-range [] (range -1 2))

(defn neighbour-cell? [delta-x delta-y]
  (not (and (zero? delta-x) (zero? delta-y))))

(defn underpopulated? [num-neighbours]
  (< num-neighbours 2))

(defn overpopulated? [num-neighbours]
  (> num-neighbours 3))

(defn survived? [[_ num-neighbours]]
  (not (or (underpopulated? num-neighbours) (overpopulated? num-neighbours))))

(defn reproduced? [[_ num-neighbours]]
  (= num-neighbours 3))

(defn next-generation [live-cells]
  (let [num-neighbours-live-cells (atom {})
        num-neighbours-dead-cells (atom {})]
    (doseq [live-cell live-cells]
      (doseq [delta-x (delta-range) delta-y (delta-range)
              :when (neighbour-cell? delta-x delta-y)]
        (let [[x y] live-cell
               neighbour-cell [(+ x delta-x) (+ y delta-y)]]
          (if (live-cell? live-cells neighbour-cell)
            (swap! num-neighbours-live-cells assoc live-cell
                    (+ (@num-neighbours-live-cells live-cell) 1))
            (swap! num-neighbours-dead-cells assoc neighbour-cell
                    (+ (@num-neighbours-dead-cells neighbour-cell) 1))))))
    (let [survived-cells (->> @num-neighbours-live-cells (filter survived?) keys set)
          reproduced-cells (->> @num-neighbours-dead-cells (filter reproduced?) keys set)]
      (union survived-cells reproduced-cells))))

(defn draw [f coordinate]
  (let [[x y] coordinate]
    (.call f @context (* x 10) (* y 10) 10 10)))

(defn draw-erase-cell [coordinate]
  (set! (.-fillStyle @context) "white")
  (draw (. @context -fillRect) coordinate))

(defn draw-live-cell [coordinate]
  (set! (.-fillStyle @context) "black")
  (draw (. @context -fillRect) coordinate))

(defn draw-dead-cell [coordinate]
  (draw-erase-cell coordinate)
  (draw (. @context -strokeRect) coordinate))

(defn clear-recently-dead-cells [dead-cells]
  (doseq [dead-cell dead-cells] (draw-dead-cell dead-cell)))

(defn draw-live-cells []
  (doseq [live-cell @live-cells] (draw-live-cell live-cell)))

(defn draw-empty-board []
  (doseq [x (range 0 100)
          y (range 0 100)]
    (draw-dead-cell [x y])))

(defn redraw-board []
  (draw-empty-board)
  (draw-live-cells))

(defn run-next-generation []
  (let [dead-cells (difference @live-cells (swap! live-cells next-generation))]
    (clear-recently-dead-cells dead-cells)
    (draw-live-cells)))

(defn start-simulation []
  (let [{start :start stop :stop next-gen :next-gen} @buttons]
    (doseq [button [start next-gen]]
      (set! (.-disabled button) true))
    (set! (.-disabled stop) false)
    (reset! simulation (js/setInterval run-next-generation simulation-speed))))

(defn stop-simulation []
  (let [{start :start stop :stop next-gen :next-gen} @buttons]
    (doseq [button [start next-gen]]
      (set! (.-disabled button) false))
    (set! (.-disabled stop) true)
    (js/clearInterval @simulation)
    (reset! simulation nil)))

(defn load-initial-state [event]
  (let [file (first (.-files (.-target event)))
        fr (js/FileReader.)]
    (.readAsText fr file)
    (set! (.-onloadend fr)
          (fn []
            (->> (.-result fr) js/JSON.parse js->clj (apply sorted-set) (reset! live-cells))
            (redraw-board)))))

(defn save-current-state []
  (let [blob (new js/Blob. [(->> @live-cells clj->js js/JSON.stringify)] {:type "application/json"})]
    (.saveAs file-saver blob "conway-state.json")))

(defn attach-listeners []
  (let [{start :start
         stop :stop
         next-gen :next-gen
         load-state :load-state
         save-state :save-state} @buttons]
    (.addEventListener start "click" start-simulation)
    (.addEventListener stop "click" stop-simulation)
    (.addEventListener next-gen "click" run-next-generation)
    (.addEventListener load-state "change" load-initial-state)
    (.addEventListener save-state "click" save-current-state)))

(defn get-buttons []
  (reset! buttons {:start (js/document.getElementById "start")
                   :stop (js/document.getElementById "stop")
                   :next-gen (js/document.getElementById "next-gen")
                   :load-state (js/document.getElementById "load-state")
                   :save-state (js/document.getElementById "save-state")}))

(defn main [& args]
  (reset! context (.getContext js/canvas "2d"))
  (draw-empty-board)
  (get-buttons)
  (attach-listeners))

(. js/document addEventListener "DOMContentLoaded" main)
