# Conway's Game of Life

## Setup and Run

```sh
$ npm ci
$ npm run start
```

## Emacs Cider Dev

1. Jack-in command:

```
C-c C-x j s
```

2. Choose "shadow"
3. Choose yes to go to build site (http://localhost:9630) to view live build
4. Goto http://localhost:8000 to view live preview


To eval forms in editor:

```
C-x C-e
```
